-- Step 1: Create a database with a domain-related name
CREATE DATABASE CarDB;

-- Step 2: Create separate schemas for cars and owners
CREATE SCHEMA Cars;
CREATE SCHEMA Owners;

-- Step 3: Create tables based on the 3NF model
-- In this example, we'll create tables for cars and owners
-- Cars Table
CREATE TABLE Cars.Car (
    car_id SERIAL PRIMARY KEY,
    make VARCHAR(50) NOT NULL,
    model VARCHAR(50) NOT NULL,
    year INT CHECK (year >= 2000),
    color VARCHAR(20),
    record_ts TIMESTAMP DEFAULT current_date
);

-- Owners Table
CREATE TABLE Owners.Owner (
    owner_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    birthdate DATE CHECK (birthdate > '1980-01-01'),
    record_ts TIMESTAMP DEFAULT current_date
);

-- Step 4: Create relationships using primary and foreign keys
-- Establish a relationship between cars and owners
ALTER TABLE Cars.Car
ADD COLUMN owner_id INT REFERENCES Owners.Owner(owner_id);

-- Step 5: Apply check constraints
-- Additional check constraints (not null and unique) are automatically satisfied
-- Year and birthdate constraints are already applied in the table definitions

-- Step 6: Populate the tables with sample data
INSERT INTO Cars.Car (make, model, year, color, owner_id)
VALUES
    ('Toyota', 'Camry', 2020, 'Blue', 1),
    ('Honda', 'Civic', 2018, 'Red', 2);

INSERT INTO Owners.Owner (first_name, last_name, birthdate)
VALUES
    ('John', 'Doe', '1985-04-15'),
    ('Jane', 'Smith', '1990-08-20');

-- Step 7: Add 'record_ts' field to each table using ALTER TABLE
-- and ensure the default value is set for existing rows
ALTER TABLE Cars.Car
ADD COLUMN record_ts TIMESTAMP DEFAULT current_date;

ALTER TABLE Owners.Owner
ADD COLUMN record_ts TIMESTAMP DEFAULT current_date;

-- Verify the default values are set for existing rows
UPDATE Cars.Car SET record_ts = current_date;
UPDATE Owners.Owner SET record_ts = current_date;
